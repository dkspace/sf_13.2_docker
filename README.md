# 13.2 GitLab pipeline ci/cd with docker
This is an exercise for DevOps training from [SkillFactory.ru - DEVOPS](https://lms.skillfactory.ru/)


### webinar Docker

    docker build -t podinfo
    docker run --rm -d --name podinfo -p 8080:80 podinfo


#### Clone Deockerfile repo to GitLab

Deploy token

1. Settings->Repository

    gitlab+deploy-token-514526 
    TF23ezJCYc3MiB4FFCxH

2. Setting->CI/CD

Variables add

    GIRLAB_CI_PASSWORD TF23ezJCYc3MiB4FFCxH
    GITLAB_CI_USER gitlab+deploy-token-514526



Mount local folder into container (option -v)

    docker run --rm -d --name podinfo -v $(pwd):/www/php -p 8080:80 podinfo


## [link to Gitlab-runner in docker](https://docs.gitlab.com/runner/install/docker.html)
	
Case with volume
        
        docker volume create gitlab-runner-config

        docker run -d --name gitlab-runner --restart always \
            -v /var/run/docker.sock:/var/run/docker.sock \
            -v gitlab-runner-config:/etc/gitlab-runner \
            gitlab/gitlab-runner:latest

Register Runner (tick off Shared runner - as it`s payed)
    
    docker run --rm -it -v gitlab-runner-config:/etc/gitlab-runner gitlab/gitlab-runner:latest register 


=========================================================================================================================== 
```shell
(base) [dmik@dkhost ~]$ docker run --rm -it -v gitlab-runner-config:/etc/gitlab-runner gitlab/gitlab-runner:latest register
Runtime platform                                    arch=amd64 os=linux pid=7 revision=c1edb478 version=14.0.1
Running in system-mode.                             
Enter the GitLab instance URL (for example, https://gitlab.com/):
https://gitlab.com/
Enter the registration token:
NGUUCCHNapc_xCz4cyE2
Enter a description for the runner:
[d2710df859f1]: inDocker_13.2
Enter tags for the runner (comma-separated):
inDocker
Registering runner... succeeded                     runner=NGUUCCHN
Enter an executor: kubernetes, docker, docker-ssh, parallels, ssh, docker+machine, docker-ssh+machine, custom, shell, virtualbox:
shell
Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded! 
(base) [dmik@dkhost ~]$ 
(base) [dmik@dkhost ~]$ 
(base) [dmik@dkhost ~]$ 
(base) [dmik@dkhost ~]$ docker ps -a
CONTAINER ID   IMAGE                         COMMAND                  CREATED          STATUS                  PORTS     NAMES
afce9936ba7a   gitlab/gitlab-runner:latest   "/usr/bin/dumb-init …"   10 minutes ago   Up 10 minutes                     gitlab-runner
799238164c8f   b66db615f86f                  "/bin/sh -c 'wget -P…"   2 days ago       Exited (0) 2 days ago             musing_varahamihira
4b3307773819   b66db615f86f                  "/bin/sh -c 'wget -P…"   2 days ago       Exited (0) 2 days ago             nifty_joliot
```
===============================================================================================================================

```shell

privileged = true
volumes = ["/var/run/docker.sock:/var/run/docker.sock", "/cache"]

[root@dkhost _data]# pwd
/var/lib/docker/volumes/gitlab-runner-config/_data
[root@dkhost _data]# cat config.toml 
concurrent = 1
check_interval = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "6d2a49ea5189"
  url = "https://gitlab.com/"
  token = "LEwwmbMUNYtEWK-3m9p4"
  executor = "docker"
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
  [runners.docker]
    tls_verify = false
    image = "ubuntu:20.10"
    privileged = true
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/var/run/docker.sock:/var/run/docker.sock", "/cache"]
    shm_size = 0
[root@dkhost _data]# 

```
